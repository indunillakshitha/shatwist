<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');


// Route::get('/', function () {
//     return view('front.index');
// });
Route::get('/shop','ShopController@index')->name('shop.index');



Route::get('/admin','DashboardController@index')->name('admin.index');

Route::prefix('admin')->group(function () {
Route::get('/category/all','CategoryController@index')->name('category.index');
Route::get('/category/create','CategoryController@create')->name('category.create');
Route::post('/category/store','CategoryController@store')->name('category.store');



Route::get('/items/all','ItemController@index')->name('items.index');
Route::get('/items/create','ItemController@create')->name('items.create');
Route::post('/items/store','ItemController@store')->name('items.store');



Route::get('/mycake/all','MyCakeController@index')->name('mycake.index');
Route::get('/mycake/create','MyCakeController@create')->name('mycake.create');
Route::post('/mycake/store','MyCakeController@store')->name('mycake.store');

});

Route::prefix('mytransactions')->group(function () {

Route::get('/add','MyTransactionController@create')->name('mytransactions.index');
Route::post('/store','MyTransactionController@store')->name('mytransactions.store');


});
Route::prefix('shop')->group(function () {

Route::get('/','ShopController@index')->name('shop.index');
Route::post('/store','MyTransactionController@store')->name('mytransactions.store');




});

//common
Route::post('/getsubcategories','CategoryController@getSub')->name('get.subCat');
