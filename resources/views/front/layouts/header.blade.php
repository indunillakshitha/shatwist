<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="{{asset('front/img/fav-icon.png')}}" type="image/x-icon" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Cake - Bakery</title>

    <!-- Icon css link -->
    <link href="{{asset('front/css/font-awesome.min.css')}}" rel="stylesheet" />
    <link
      href="{{asset('front/vendors/linearicons/style.css')}}"
      rel="stylesheet"
    />
    <link
      href="{{asset('front/vendors/flat-icon/flaticon.css')}}"
      rel="stylesheet"
    />
    <!-- Bootstrap -->
    <link href="{{asset('front/css/bootstrap.min.css')}}" rel="stylesheet" />

    <!-- Rev slider css -->
    <link
      href="{{asset('front/vendors/revolution/css/settings.css')}}"
      rel="stylesheet"
    />
    <link
      href="{{asset('front/vendors/revolution/css/layers.css')}}"
      rel="stylesheet"
    />
    <link
      href="{{asset('front/vendors/revolution/css/navigation.css')}}"
      rel="stylesheet"
    />
    <link
      href="{{asset('front/vendors/animate-css/animate.css')}}"
      rel="stylesheet"
    />

    <!-- Extra plugin css -->
    <link
      href="{{asset('front/vendors/owl-carousel/owl.carousel.min.css')}}"
      rel="stylesheet"
    />
    <link
      href="{{asset('front/vendors/magnifc-popup/magnific-popup.css')}}"
      rel="stylesheet"
    />
    <link
      href="{{asset('front/vendors/nice-select/css/nice-select.css')}}"
      rel="stylesheet"
    />

    <link href="{{asset('front/css/style.css')}}" rel="stylesheet" />
    <link href="{{asset('front/css/responsive.css')}}" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>