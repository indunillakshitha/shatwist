@extends('back.app')

@section('content')

                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles bg-dark" >
                    <div class="col-md-12">
                        <h4 class="text-white">Categories</h4>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Categories</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Categories</a></li>
                                <li class="breadcrumb-item active">All</li>
                            </ol>
                            <a href="{{route('category.create')}}"class="btn btn-info d-none d-lg-block m-l-15"><i
                                    class="fa fa-plus-circle"></i> Create New</a>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Category</th>
                                                <th>Has Parent</th>
                                                <th>Is Active</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Category</th>
                                                <th>Has Parent</th>
                                                <th>Is Active</th>
                                                <th>Actions</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        @foreach ($categories as $category )
                                            <tr>    
                                                <td>{{$category->id}}</td>
                                                <td>{{$category->name}}</td>
                                                @if ($category->parent_id==0)
                                                <td><span class="label label-danger">PARENT</span></td>
                                                @else
                                                <td><span class="label label-info">SUB</span></td>
                                                @endif
                                                <td
                                                >@if($category->is_active==1)
                                                <span class="label label-info">ACTIVE</span>
                                                @else
                                                <span class="label label-danger">INACTIVE</span>
                                                @endif
                                                </td>
                                                <td></td>
                                            </tr>
                                         @endforeach
                                                
                                         
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->



@endsection