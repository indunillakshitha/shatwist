{{-- <!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Dashboard</title>

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <!-- Perfect Scrollbar -->
    <link type="text/css" href={{asset("/UI//vendor/perfect-scrollbar.css")}} rel="stylesheet">

    <!-- App CSS -->
    <link type="text/css" href={{asset("/UI//css/app.css")}} rel="stylesheet">
    <link type="text/css" href={{asset("/UI//css/app.rtl.css")}} rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href={{asset("/UI//css/vendor-material-icons.css")}} rel="stylesheet">
    <link type="text/css" href={{asset("/UI//css/vendor-material-icons.rtl.css")}} rel="stylesheet">

    <!-- Font Awesome FREE Icons -->
    <link type="text/css" href={{asset("/UI//css/vendor-fontawesome-free.css")}} rel="stylesheet">
    <link type="text/css" href={{asset("/UI//css/vendor-fontawesome-free.rtl.css")}} rel="stylesheet">

    <!-- ion Range Slider -->
    <link type="text/css" href={{asset("/UI//css/vendor-ion-rangeslider.css")}} rel="stylesheet">
    <link type="text/css" href={{asset("/UI//css/vendor-ion-rangeslider.rtl.css")}} rel="stylesheet">


    <!-- Flatpickr -->
    <link type="text/css" href={{asset("/UI//css/vendor-flatpickr.css")}} rel="stylesheet">
    <link type="text/css" href={{asset("/UI//css/vendor-flatpickr.rtl.css")}} rel="stylesheet">
    <link type="text/css" href={{asset("/UI//css/vendor-flatpickr-airbnb.css")}} rel="stylesheet">
    <link type="text/css" href={{asset("/UI//css/vendor-flatpickr-airbnb.rtl.css")}} rel="stylesheet">

    <!-- Toastr -->
    <link type="text/css" href={{asset("/UI/vendor/toastr.min.css")}} rel="stylesheet">

    <!-- Vector Maps -->
    <link type="text/css" href={{asset("/UI//vendor/jqvmap/jqvmap.min.css")}} rel="stylesheet">

    swal
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js"></script>

    jQuery
    <script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <script>
        $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})
    </script>



</head> --}}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('UI3/images/favicon.png')}}">
    <title>SHATWIST ADMIN</title>
    <!-- Custom CSS -->
    <!-- toast CSS -->
    <link href={{asset("/UI3/node_modules/toast-master/css/jquery.toast.css")}} rel="stylesheet">
    <!-- Custom CSS -->
    <link href={{asset("/UI3/css/style.min.css")}} rel="stylesheet">
    <!-- page css -->
    <link href={{asset("/UI3/css/pages/other-pages.css")}} rel="stylesheet">
    <link href={{asset("/UI3/css/pages/floating-label.css")}} rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

{{-- jQuery --}}
<script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>


    <script>
        $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})
    </script>

{{-- DataTables --}}
<link rel="stylesheet" type="text/css"
href={{asset("/UI3/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css")}}>
<link rel="stylesheet" type="text/css"
href={{asset("/UI3/node_modules/datatables.net-bs4/css/responsive.dataTables.min.css")}}>

<!-- This is data table -->
<script src={{asset("/UI3/node_modules/datatables.net/js/jquery.dataTables.min.js")}}></script>
<script src={{asset("/UI3/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js")}}></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

<link href={{asset("/UI3/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css")}} rel="stylesheet">

<link href={{asset("/UI3/node_modules/clockpicker/dist/jquery-clockpicker.min.css")}} rel="stylesheet">

<!-- Date picker plugins css -->
<link href={{asset("/UI3/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css")}} rel="stylesheet" type="text/css" />

{{-- swal --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js"></script>



</head>

<body class="skin-blue-dark fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">SHA TWIST ADMIN</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
