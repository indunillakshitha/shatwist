<!-- footer -->
<!-- ============================================================== -->

<footer class="footer">
    © 2019 Eliteadmin by themedesigner.in
</footer>
<!-- ============================================================== -->
<!-- End footer -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src={{asset("/UI3/node_modules/jquery/jquery-3.2.1.min.js")}}></script>
<!-- Bootstrap tether Core JavaScript -->
<script src={{asset("/UI3/node_modules/popper/popper.min.js")}}></script>
<script src={{asset("/UI3/node_modules/bootstrap/dist/js/bootstrap.min.js")}}></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src={{asset("/UI3/js/perfect-scrollbar.jquery.min.js")}}></script>
<!--Wave Effects -->
<script src={{asset("/UI3/js/waves.js")}}></script>
<!--Menu sidebar -->
<script src={{asset("/UI3/js/sidebarmenu.js")}}></script>
<!--stickey kit -->
<script src={{asset("/UI3/node_modules/sticky-kit-master/dist/sticky-kit.min.js")}}></script>
<script src={{asset("/UI3/node_modules/sparkline/jquery.sparkline.min.js")}}></script>
<!--Custom JavaScript -->
<script src={{asset("/UI3/js/custom.min.js")}}></script>

<!-- toastr -->
<script src={{asset("/UI3/node_modules/toast-master/js/jquery.toast.js")}}></script>
<script src={{asset("/UI3/js/pages/toastr.js")}}></script>

<script src={{asset("/UI3/js/pages/jasny-bootstrap.js")}}></script>

<script src={{asset("/UI3/js/pages/validation.js")}}></script>
<script>
    ! function(window, document, $) {
        "use strict";
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }(window, document, jQuery);
</script>

<!-- Plugin JavaScript -->
<script src={{asset("/UI3/node_modules/moment/moment.js")}}></script>
<script src={{asset("/UI3/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js")}}>
</script>


<!-- Clock Plugin JavaScript -->
<script src={{asset("/UI3/node_modules/clockpicker/dist/jquery-clockpicker.min.js")}}></script>

<!-- Form Masks -->
<script src={{asset("/UI3/node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js")}}></script>
<script src={{asset("/UI3/js/pages/mask.init.js")}}></script>

<!-- Date Picker Plugin JavaScript -->
<script src={{asset("/UI3/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js")}}></script>

<script>
    // MAterial Date picker
    $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });

    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('.clockpicker').clockpicker({
        donetext: 'Done',
    }).find('input').change(function() {
        // console.log(this.value);
    });
</script>

<script>
    //get sub category by sending category id
    function getCat(cat_id){
        const sub_cat = document.querySelector('#sub_category_id')
        $.ajax({
                        type:'POST',
                        url:'/getsubcategories',
                        data:{
                            "_token": "{{ csrf_token() }}",
                            cat_id: cat_id,


                        },
                        success:function(data){
                           sub_cat.innerHTML = `
                        <select name="sub_category_id" class="form-control" id="sub_category_id"> Select </select>
                        `
                        data.forEach(record => {
                            html = `
                            <option id="sub_category_id" name="sub_category_id" value="${record.id}">${record.name}</option>
                            `

                            sub_cat.innerHTML += html

                        })
                     }
            });

        }


        function show_selected_image(){
            // console.log(event.target.files)
            const files = event.target.files

            const fileReader = new FileReader()
            fileReader.addEventListener('load', ()=>{
                selected_image.src = fileReader.result
            })
            fileReader.readAsDataURL(files[0])

            remove_selected_img_btn.classList.remove('d-none')
        }

        function remove_selected_image(){
            selected_image.src = null
            img_input.value = null
            remove_selected_img_btn.classList.add('d-none')
        }

        function alert_delete(id, confirmButtonText, url){
            Swal.fire({
                title: 'Are you sure ?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                showCloseButton: true,
                // cancelButtonColor: '#d33',
                confirmButtonText,
                })
                .then((result) => {
                    // console.log(result);
                if (result.value ==true) {
                    // return handle_approval_type(id, is_approved)
                    location = url
                }
            })

        }
</script>
<!-- This is data table -->
<script src="{{asset('/UI3/node_modules/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('/UI3/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js')}}"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->
<script>
    $(function () {
         $('#myTable').DataTable();
         var table = $('#example').DataTable({
             "columnDefs": [{
                 "visible": false,
                 "targets": 2
             }],
             "order": [
                 [2, 'asc']
             ],
             "displayLength": 25,
             "drawCallback": function (settings) {
                 var api = this.api();
                 var rows = api.rows({
                     page: 'current'
                 }).nodes();
                 var last = null;
                 api.column(2, {
                     page: 'current'
                 }).data().each(function (group, i) {
                     if (last !== group) {
                         $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                         last = group;
                     }
                 });
             }
         });
         // Order by the grouping
         $('#example tbody').on('click', 'tr.group', function () {
             var currentOrder = table.order()[0];
             if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                 table.order([2, 'desc']).draw();
             } else {
                 table.order([2, 'asc']).draw();
             }
         });
         // responsive table
         $('#config-table').DataTable({
             responsive: true
         });
         $('#example23').DataTable({
             dom: 'Bfrtip',
             buttons: [
                 'copy', 'csv', 'excel', 'pdf', 'print'
             ]
         });
         $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
     });

</script>

</body>

</html>
