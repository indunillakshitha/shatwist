@extends('back.app')

@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">New My Cake</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">My Cake</a></li>
                <li class="breadcrumb-item active">New</li>
            </ol>
            <a href="{{route('mycake.index')}}" class="btn btn-info d-none d-lg-block m-l-15"><i
                    class="fa fa-plus-circle"></i> Back</a>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Cake Details</h4>
            <form class="needs-validation" action="{{route('mycake.store')}}" method="POST" novalidate
                enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="validationCustom01">Category name</label>
                        <select class="custom-select" required name="category_id" onclick="getCat(this.value)">
                            <option value="0">No Parent</option>
                            @foreach ($categories as $category )
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach

                        </select>
                        <div class="invalid-feedback">Example invalid custom select feedback</div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationCustom01">Sub Category Name</label>
                        <select class="custom-select" name="sub_category_id" id="sub_category_id">
                        </select>
                        <div class="invalid-feedback">Example invalid custom select feedback</div>
                    </div>

                </div>
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="validationCustom01">Item Name</label>
                        <input type="text" class="form-control" id="item_name" name="item_name" placeholder="Item name"
                            value="" required>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="item_description">Item description</label>
                        <textarea class="form-control" id="item_description" name="item_description" rows="4"
                            placeholder="Description"></textarea>

                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="item_cost">Item Cost</label>
                        <input type="number" class="form-control" id="item_cost" name="item_cost"
                            placeholder="Item name" value="" required>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="item_price">Item price</label>
                        <input type="number" class="form-control" id="item_price" name="item_price"
                            placeholder="Item price" value="" required>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="item_qty">Item Qty</label>
                        <input type="number" class="form-control" id="item_qty" name="item_qty" placeholder="Item qty"
                            value="" required>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>

                </div>
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="item_image">main Image</label>
                        <input type="file" class="form-control" id="item_image" name="item_image" required>
                        <div class="valid-feedback">
                            Looks good!
                        </div>
                    </div>


                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustom01">Is Featured</label>
                    <select class="custom-select" required name="is_featured">
                        <option value="0">No</option>
                        <option value="1">Yes</option>

                    </select>
                </div>
                <button class="btn btn-primary" type="submit">Create </button>
            </form>
            <script>
                // Example starter JavaScript for disabling form submissions if there are invalid fields
            (function() {
                'use strict';
                window.addEventListener('load', function() {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function(form) {
                        form.addEventListener('submit', function(event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
            </script>
        </div>
    </div>
</div>


@endsection
