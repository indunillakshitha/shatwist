<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ADD</title>

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <!-- Perfect Scrollbar -->
    <link type="text/css" href={{asset("/UI/vendor/perfect-scrollbar.css")}} rel="stylesheet">

    <!-- App CSS -->
    <link type="text/css" href={{asset("/UI/css/app.css")}} rel="stylesheet">
    <link type="text/css" href={{asset("/UI/css/app.rtl.css")}} rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href={{asset("/UI/css/vendor-material-icons.css")}} rel="stylesheet">
    <link type="text/css" href={{asset("/UI/css/vendor-material-icons.rtl.css")}} rel="stylesheet">

    <!-- Font Awesome FREE Icons -->
    <link type="text/css" href={{asset("/UI/css/vendor-fontawesome-free.css")}} rel="stylesheet">
    <link type="text/css" href={{asset("/UI/css/vendor-fontawesome-free.rtl.css")}} rel="stylesheet">

    <!-- ion Range Slider -->
    <link type="text/css" href={{asset("/UI/css/vendor-ion-rangeslider.css")}} rel="stylesheet">
    <link type="text/css" href={{asset("/UI/css/vendor-ion-rangeslider.rtl.css")}} rel="stylesheet">


    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js"></script>


</head>

<body class="layout-login-centered-boxed mt-5">





    <div class="layout-login-centered-boxed__form ">
        <div class="d-flex flex-column justify-content-center align-items-center mt-2 mb-4 navbar-light">
            <a href="index.html" class="navbar-brand text-center mb-2 mr-0 flex-column" style="min-width: 0">
                <!-- LOGO -->


                <span class="mt-2">ADD</span>
            </a>
        </div>
        <div class="card card-body mt-5">


            <form action="/register" method="POST" class="mt-5">
                @csrf
                <div class="form-group">
                    {{-- <label class="text-label" for="name_2">Category :</label> --}}
                    <div class="input-group input-group-merge">
                        <select class="custom-select" required name="trans_type" id="trans_type">
                            <option value="EXPENCE">EXPENCE</option>
                            <option value="INCOME">INCOME</option>
                            <option value="GOT FROM">GOT FROM</option>
                            <option value="GIVE TO">GIVE TO</option>

                        </select>
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="far fa-user"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{-- <label class="text-label" for="name_2">REASON :</label> --}}
                    <div class="input-group input-group-merge">
                        <select class="custom-select" required name="reason_category" id="reason_category">
                            <option value="OTHER">OTHER</option>
                            <option value="FOOD">FOOD</option>
                            <option value="FUEL">FUEL</option>


                        </select>
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="far fa-user"></span>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- @error('email')
                <div class="alert alert-danger d-flex" role="alert">
                        <i class="material-icons mr-3">error_outline</i>
                        <div class="text-body"><strong>{{ $message }}</strong>
        </div>
    </div>
    @enderror --}}

    <div class="form-group">
        {{-- <label class="text-label" for="email_2">VALUE</label> --}}
        <div class="input-group input-group-merge">
            <input id="value" type="number" class="form-control form-control-prepended" placeholder="Value" name="value"
                value="{{ old('value') }}" required>
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="far fa-envelope"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        {{-- <label class="text-label" for="email_2">VALUE</label> --}}
        <div class="input-group input-group-merge">
            <input id="reason_details" type="text" class="form-control form-control-prepended" placeholder="Descrption"
                name="reason_details">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="far fa-settings"></span>
                </div>
            </div>
        </div>
    </div>


    <div class="form-group text-center">
        <button class="btn btn-primary mb-2" type="button" onclick="saveTrans()">ADD</button><br>
        <span>Have an account?</span> <a class="text-body text-underline" href="/login"> HOME</a>
    </div>
    </form>
    </div>
    </div>



    <!-- jQuery -->
    <script src={{asset("/UI/vendor/jquery.min.js")}}></script>

    <!-- Bootstrap -->
    <script src={{asset("/UI/vendor/popper.min.js")}}></script>
    <script src={{asset("/UI/vendor/bootstrap.min.js")}}></script>

    <!-- Perfect Scrollbar -->
    <script src={{asset("/UI/vendor/perfect-scrollbar.min.js")}}></script>

    <!-- DOM Factory -->
    <script src={{asset("/UI/vendor/dom-factory.js")}}></script>

    <!-- MDK -->
    <script src={{asset("/UI/vendor/material-design-kit.js")}}></script>

    <!-- Range Slider -->
    <script src={{asset("/UI/vendor/ion.rangeSlider.min.js")}}></script>
    <script src={{asset("/UI/js/ion-rangeslider.js")}}></script>

    <!-- App -->
    <script src={{asset("/UI/js/toggle-check-all.js")}}></script>
    <script src={{asset("/UI/js/check-selected-row.js")}}></script>
    <script src={{asset("/UI/js/dropdown.js")}}></script>
    <script src={{asset("/UI/js/sidebar-mini.js")}}></script>
    <script src={{asset("/UI/js/app.js")}}></script>

    <script type="text/javascript">
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            function saveTrans(){

            var reason_category =document.getElementById('reason_category').value;
            var trans_type =document.getElementById('trans_type').value;
            var value =document.getElementById('value').value;
            var reason_details =document.getElementById('reason_details').value;

            if(!reason_category || !trans_type || !value ){
               return Swal.fire({
                            type: 'errr',
                            title: 'Please Fill All details',
                            text: '',
                            })
            }

            $.ajax({
                        type:'POST',
                        url:'/mytransactions/store',
                        data:{
                            "_token": "{{ csrf_token() }}",
                            reason_category: reason_category,
                            trans_type: trans_type,
                            value: value,
                            reason_details: reason_details,

                        },
                        success:function(data){
                           console.log(data)
                           Swal.fire({
                            type: 'success',
                            title: 'vh',
                        // text: 'Use Following Link to check Status',
                        // footer: '<a href ="/ticket/view">check status</a>'
                            })
                        .then((result) => {
                        location.reload();

                            })
                        }
                    });

        }
    </script>
</body>

</html>
