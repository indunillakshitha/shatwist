<!DOCTYPE html>
<html lang="en" dir="ltr">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Login</title>

        <!-- Prevent the demo from appearing in search engines -->
        <meta name="robots" content="noindex">

        <!-- Perfect Scrollbar -->
        <link type="text/css" href={{asset("/UI/vendor/perfect-scrollbar.css")}} rel="stylesheet">

        <!-- App CSS -->
        <link type="text/css" href={{asset("/UI/css/app.css")}} rel="stylesheet">
        <link type="text/css" href={{asset("/UI/css/app.rtl.css")}} rel="stylesheet">

        <!-- Material Design Icons -->
        <link type="text/css" href={{asset("/UI/css/vendor-material-icons.css")}} rel="stylesheet">
        <link type="text/css" href={{asset("/UI/css/vendor-material-icons.rtl.css")}} rel="stylesheet">

        <!-- Font Awesome FREE Icons -->
        <link type="text/css" href={{asset("/UI/css/vendor-fontawesome-free.css")}} rel="stylesheet">
        <link type="text/css" href={{asset("/UI/css/vendor-fontawesome-free.rtl.css")}} rel="stylesheet">

        <!-- ion Range Slider -->
        <link type="text/css" href={{asset("/UI/css/vendor-ion-rangeslider.css")}} rel="stylesheet">
        <link type="text/css" href={{asset("/UI/css/vendor-ion-rangeslider.rtl.css")}} rel="stylesheet">





    </head>

<body class="layout-login-centered-boxed">





    <div class="layout-login-centered-boxed__form">
        <div class="d-flex flex-column justify-content-center align-items-center mt-2 mb-4 navbar-light">
            <a href="index.html" class="navbar-brand text-center mb-2 mr-0 flex-column" style="min-width: 0">
                <!-- LOGO -->


                <span class="mt-2">Register</span>
            </a>
        </div>
        <div class="card card-body">


            <form action="/register" method="POST">
                @csrf
                <div class="form-group">
                    <label class="text-label" for="name_2">Name:</label>
                    <div class="input-group input-group-merge">
                        <input id="name_2" type="text"  class="form-control form-control-prepended"
                        placeholder="Enter your name"
                         name="name" value="{{ old('name') }}" required autocomplete="name" autofocus
                        >
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="far fa-user"></span>
                            </div>
                        </div>
                    </div>
                </div>

                @error('email')
                <div class="alert alert-danger d-flex" role="alert">
                        <i class="material-icons mr-3">error_outline</i>
                        <div class="text-body"><strong>{{ $message }}</strong></div>
                </div>
                @enderror

                <div class="form-group">
                    <label class="text-label" for="email_2">Email Address:</label>
                    <div class="input-group input-group-merge">
                        <input id="email_2" type="email" class="form-control form-control-prepended" placeholder="Email"
                        name="email" value="{{ old('email') }}" required autocomplete="email"
                        >
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="far fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                </div>

                @error('email')
                <div class="alert alert-danger d-flex" role="alert">
                        <i class="material-icons mr-3">error_outline</i>
                        <div class="text-body"><strong>{{ $message }}</strong></div>
                </div>
                @enderror

                <div class="form-group">
                    <label class="text-label" for="password_2">Password:</label>
                    <div class="input-group input-group-merge">
                        <input id="password_2" type="password"  class="form-control form-control-prepended"
                        placeholder="Password"
                        name="password" required autocomplete="new-password"
                        >
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="fa fa-key"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="text-label" for="password_2">Confirm Password:</label>
                    <div class="input-group input-group-merge">
                        <input id="password_2" type="password"  class="form-control form-control-prepended"
                        placeholder="Confirm Password"
                        name="password_confirmation" required autocomplete="new-password"
                        >
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="fa fa-key"></span>
                            </div>
                        </div>
                    </div>
                </div>

                @error('password')
                <div class="alert alert-danger d-flex" role="alert">
                        <i class="material-icons mr-3">error_outline</i>
                        <div class="text-body"><strong>{{ $message }}</strong></div>
                </div>
                @enderror


                <div class="form-group mb-3 text-center">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" checked="" class="custom-control-input" id="terms" />
                        <label class="custom-control-label" for="terms">I accept <a href="#">Terms and Conditions</a></label>
                    </div>
                </div>
                <div class="form-group text-center">
                    <button class="btn btn-primary mb-2" type="submit">Register</button><br>
                    <span>Have an account?</span> <a class="text-body text-underline" href="/login"> Login</a>
                </div>
            </form>
        </div>
    </div>



     <!-- jQuery -->
     <script src={{asset("/UI/vendor/jquery.min.js")}}></script>

     <!-- Bootstrap -->
     <script src={{asset("/UI/vendor/popper.min.js")}}></script>
     <script src={{asset("/UI/vendor/bootstrap.min.js")}}></script>

     <!-- Perfect Scrollbar -->
     <script src={{asset("/UI/vendor/perfect-scrollbar.min.js")}}></script>

     <!-- DOM Factory -->
     <script src={{asset("/UI/vendor/dom-factory.js")}}></script>

     <!-- MDK -->
     <script src={{asset("/UI/vendor/material-design-kit.js")}}></script>

     <!-- Range Slider -->
     <script src={{asset("/UI/vendor/ion.rangeSlider.min.js")}}></script>
     <script src={{asset("/UI/js/ion-rangeslider.js")}}></script>

     <!-- App -->
     <script src={{asset("/UI/js/toggle-check-all.js")}}></script>
     <script src={{asset("/UI/js/check-selected-row.js")}}></script>
     <script src={{asset("/UI/js/dropdown.js")}}></script>
     <script src={{asset("/UI/js/sidebar-mini.js")}}></script>
     <script src={{asset("/UI/js/app.js")}}></script>


</body>

</html>
