<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMyTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reason_category',50)->default('NO');
            $table->string('reason_details',100)->default('NO');
            $table->string('trans_type',20)->default('EXPENCE');
            $table->decimal('value')->default(0);
            $table->tinyInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_transactions');
    }
}
