<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('items', function (Blueprint $table) {

        $table->bigInteger('category_id')->unsigned()->index();
        $table->foreign('category_id')->references('id')->on('categories');

        $table->bigInteger('sub_category_id')->default('0');
        ;
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
