<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable =[
        'item_code',
'item_batch_no',
'item_name',
'item_description',
'item_image',
'item_cost',
'item_price',
'item_qty',
'user_id',
'parent_id',
'is_active',
'category_id',
'sub_category_id',

    ];



   
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
