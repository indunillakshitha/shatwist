<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyTransaction extends Model
{
    protected $fillable = [
        'reason_category',
        'reason_details',
        'trans_type',
        'value',
        'user_id',
    ];
}
