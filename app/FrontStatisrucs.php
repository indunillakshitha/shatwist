<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrontStatisrucs extends Model
{
    protected $fillable = [
        'device_type',
        'browser',
        'ip',
    ];
}
