<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyCake extends Model
{
    protected $table = 'my_cakes';
    protected $fillable = [
        'item_code',
        'item_name',
        'item_description',
        'item_image',
        'item_cost',
        'item_price',
        'item_qty',
        'user_id',
        'parent_id',
        'is_active',
        'is_featured',
        'category_id',
        'sub_category_id',

    ];
}
