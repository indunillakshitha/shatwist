<?php

namespace App\Http\Controllers;

use App\MyTransaction;
use Illuminate\Http\Request;

class MyTransactionController extends Controller
{
    public function index()
    {
    }

    public function create()
    {
        $latest =MyTransaction::orderBy('id', 'DESC')->first();
        return view('back.transactions.create','latest');
    }


    public function store(Request $request)
    {
        $data = $request;
        $data['user_id']='1';
        MyTransaction::create($data->all());
        return response()->json(['sussess'=>'Successfully Added']);
    }
}
