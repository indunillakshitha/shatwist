<?php

namespace App\Http\Controllers;

use App\Category;
use App\Item;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function index(){
         $items = Item::paginate(9);
         $categories =Category::all();
        return view('front.shop',compact('items','categories'));

    }
}
