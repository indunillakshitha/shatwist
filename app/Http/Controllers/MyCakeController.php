<?php

namespace App\Http\Controllers;

use App\Category;
use App\MyCake;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MyCakeController extends Controller
{
    public function index(){
        $items =MyCake::all();
        return view('back.mycake.index',compact('items'));
    }
    
    public function create(){
        $categories =Category::all();
        return view('back.mycake.create',compact('categories'));
    }
    public function store(Request $request){
       $data =$request;
    //    dd($request);
        $category_name = Category::where('id',$request->category_id)->first()->name;

        $item_id = MyCake::max('id')+1;
        $item_code=substr($category_name, 0, 3);
        $data['item_code']=\Str::upper($item_code).$item_id;
        $data['user_id']=Auth::user()->id;
        $is_exist =MyCake::where('item_name',$request->item_name)->count();
    //   $data['item_image'];
        if($is_exist==0){
            $data['item_batch_no']='1';   
        }else{
            $data['item_batch_no']=$is_exist+1;   

        }
        $item = MyCake::create($data->all());
        
        if ($request->item_image) {
            $file = $request->File('item_image');
            $ext  = 'MYCAKE_' . $data['item_batch_no']  . time() . rand() . "." . $file->clientExtension();
            $file->move(public_path('/images/mycakes'), $ext);

            $item->item_image = $ext;
        
        } else {
            $item->item_image = 'no_image_student.jpg';
        }
       
         $item->save();

     //    return Item::create($data->all());

        return redirect()->route('mycake.create');
    }
}
