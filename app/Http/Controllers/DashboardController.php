<?php

namespace App\Http\Controllers;

use App\FrontStatisrucs;
use App\Item;
use App\MyCake;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $total_visitores = FrontStatisrucs::count('id');
        $today_visitores = FrontStatisrucs::where('created_at', Carbon::today())->count('id');
        $my_cakes = MyCake::count('id');
        $items = Item::count('id');
        return view('back.index',compact('total_visitores','today_visitores','my_cakes','items'));
    }
}
