<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){

           $categories =Category::with('parent')
          ->get();

        return view('back.categories.index',compact('categories'));
    }

    public function create(){
        $categories =Category::all();
        return view('back.categories.create',compact('categories'));
    }
    public function store(Request $request){
        $data =$request;
        Category::create($data->all());
        return redirect()->route('category.create');
    }

    public function getSub(Request $request){

        $sub_categories= Category::where('parent_id',$request->cat_id)->get();


        return response()->json($sub_categories);

    }



}
