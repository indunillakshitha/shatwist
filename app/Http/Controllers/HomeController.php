<?php

namespace App\Http\Controllers;

use App\FrontStatisrucs;
use App\MyCake;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $log = [];

    	$log['ip'] = \Request::ip();;
    	$log['browser'] = \Request::header('user-agent');
    	FrontStatisrucs::create($log);
         $featured_cakes =MyCake::orderBy('created_at','desc')->where('is_featured',1)->take(8)->get();
         $latests =MyCake::orderBy('created_at','desc')->take(4)->get();
        return view('front.index',compact('featured_cakes','latests'));
    }
}
